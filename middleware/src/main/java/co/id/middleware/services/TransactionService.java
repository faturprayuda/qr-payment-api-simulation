package co.id.middleware.services;

import co.id.middleware.dto.BackEndEntity.Token.TokenResponse;
import co.id.middleware.dto.BackEndEntity.Transaction.TransactionRequest;
import co.id.middleware.dto.BackEndEntity.Transaction.TransactionResponse;
import co.id.middleware.utils.GenerateToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransactionService {

    @Value("${url.host}")
    public String urlHost;

    private final GenerateToken generateToken;

    public ResponseEntity<Object> getTransactions(TransactionRequest transactionRequest) {
        ObjectMapper mapper = new ObjectMapper();
        String id = UUID.randomUUID().toString();
        StopWatch sw = new StopWatch();

        try {
            sw.start();
            log.info("Started Get Transactions with param : {}| REQ-ID : [{}]", mapper.writeValueAsString(transactionRequest), id);

            TokenResponse token = generateToken.getToken(id);

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
                    .build();

            RequestBody requestBody = RequestBody.create(mapper.writeValueAsString(transactionRequest), MediaType.get("application/json; charset=utf-8"));

            log.info("Prepare to Hit Host : [{}] and reqBody : {} | REQ-ID : [{}]", urlHost, mapper.writeValueAsString(requestBody), id);

            Request okHttpReq = new Request.Builder()
                    .url(urlHost)
                    .header("Authorization", "Bearer " + token.getData().getToken())
                    .post(requestBody)
                    .build();

            Call call = client.newCall(okHttpReq);
            okhttp3.Response res = call.execute();

            assert res.body() != null;
            TransactionResponse transactionResponse = mapper.readValue(res.body().string(), TransactionResponse.class);

            log.info("Success Get Data Transactions | REQ-ID : [{}]", id);

            sw.stop();
            log.info("Get Transactions --done in {}ms | REQ-ID : [{}]", sw.getTotalTimeMillis(), id);

            return ResponseEntity.ok(co.id.middleware.dto.Response.builder()
                    .page(transactionRequest.getPage())
                    .size(transactionRequest.getSize())
                    .totalSize(transactionResponse.getData().size())
                    .responseCode("200")
                    .responseMessage("success")
                    .data(transactionResponse)
                    .build());


        } catch (Exception e) {
            log.error("error: {} | REQ-ID : [{}]", e.getMessage(), id);
            return ResponseEntity.internalServerError().body(co.id.middleware.dto.Response.builder()
                    .responseCode("500")
                    .responseMessage("error")
                    .data(e.getMessage())
                    .build());
        }
    }

}
