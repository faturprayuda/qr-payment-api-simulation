package co.id.middleware.services;

import co.id.middleware.dto.BackEndEntity.Payment.PaymentRequest;
import co.id.middleware.dto.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentController {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public ResponseEntity<Object> makePayment(PaymentRequest paymentRequest) {
        StopWatch sw = new StopWatch();
        String id = UUID.randomUUID().toString();
        ObjectMapper mapper = new ObjectMapper();

        try {
            sw.start();
            log.info("Started Making Payment Request with param : {} | REQ-ID : [{}]", mapper.writeValueAsString(paymentRequest), id);

            paymentRequest.setTrxId(id);
            String reqString = mapper.writeValueAsString(paymentRequest);

            log.info("Sending data with Kafka for Saving Data | REQ-ID : [{}]", id);
            kafkaTemplate.send("my-topics", reqString);

            sw.stop();
            log.info("Payment --done in {}ms | REQ-ID : [{}]", sw.getTotalTimeMillis(), id);

            return ResponseEntity.ok(Response.builder()
                    .responseCode("200")
                    .responseMessage("success")
                    .data(paymentRequest)
                    .build());
        } catch (Exception e) {
            log.error("error: {} | REQ-ID : [{}]", e.getMessage(), id);
            return ResponseEntity.internalServerError().body(Response.builder()
                    .trxId(paymentRequest.getTrxId())
                    .responseCode("500")
                    .responseMessage("error")
                    .data(e.getMessage())
                    .build());
        }
    }

}

