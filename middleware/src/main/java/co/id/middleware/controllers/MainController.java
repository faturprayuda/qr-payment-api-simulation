package co.id.middleware.controllers;

import co.id.middleware.dto.BackEndEntity.Payment.PaymentRequest;
import co.id.middleware.dto.BackEndEntity.Transaction.TransactionRequest;
import co.id.middleware.services.PaymentController;
import co.id.middleware.services.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class MainController {

    private final PaymentController paymentController;
    private final TransactionService transactionService;

    @PostMapping("/payment")
    ResponseEntity<Object> payment(@RequestBody PaymentRequest paymentRequest){
        return paymentController.makePayment(paymentRequest);
    }

    @PostMapping("/transactions")
    ResponseEntity<Object> transactions(@RequestBody TransactionRequest transactionRequest){
        return transactionService.getTransactions(transactionRequest);
    }
}
