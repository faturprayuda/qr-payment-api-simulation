package co.id.middleware.utils;

import co.id.middleware.dto.BackEndEntity.Token.TokenResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
@RequiredArgsConstructor
public class GenerateToken {

    @Value("${url.get.token}")
    public String urlGetToken;

    public TokenResponse getToken(String reqId) throws Exception {
            ObjectMapper mapper = new ObjectMapper();
        try {

            log.info("Started Generate Token to Host : {} | REQ-ID : [{}]", urlGetToken, reqId);

//            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
//                    .addInterceptor(loggingInterceptor)
                    .build();

            Request okHttpReq = new Request.Builder()
                    .url(urlGetToken)
                    .get()
                    .build();

            Call call = client.newCall(okHttpReq);
            okhttp3.Response res = call.execute();

            TokenResponse tokenResponse = mapper.readValue(res.body().string(), TokenResponse.class);

            log.info("Success Get Token with : [{}] | REQ-ID : [{}]", tokenResponse.getData().getToken(), reqId);

            return tokenResponse;
        } catch (Exception e) {
            log.error("error get token: {} | REQ-ID : [{}]", e.getMessage(), reqId);
            throw new Exception();
        }
    }
}
