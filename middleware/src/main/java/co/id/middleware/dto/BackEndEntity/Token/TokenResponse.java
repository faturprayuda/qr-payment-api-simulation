package co.id.middleware.dto.BackEndEntity.Token;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TokenResponse {
    private String responseCode;
    private String responseMessage;
    private TokenJwt data;
}
