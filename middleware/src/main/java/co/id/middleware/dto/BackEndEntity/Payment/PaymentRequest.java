package co.id.middleware.dto.BackEndEntity.Payment;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentRequest {
    private String trxId;
    private String merchantId;
    private Integer amountPurchase;
    private String accountNumber;
    private String bankAccount;
}
