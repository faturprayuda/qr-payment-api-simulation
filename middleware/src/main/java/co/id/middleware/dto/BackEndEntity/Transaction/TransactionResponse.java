package co.id.middleware.dto.BackEndEntity.Transaction;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionResponse {
    private String responseCode;
    private String responseMessage;
    private List<Object> Data;
}
