package co.id.middleware.dto.BackEndEntity.Transaction;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionRequest {
    private String accountNumber;
    private int page;
    private int size;
}
