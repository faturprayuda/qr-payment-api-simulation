package co.id.middleware.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    private String trxId;
    private int page;
    private int size;
    private int totalSize;
    private String responseCode;
    private String responseMessage;
    private Object data;
}
