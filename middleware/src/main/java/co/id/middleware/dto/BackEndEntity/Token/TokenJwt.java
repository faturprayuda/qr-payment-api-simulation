package co.id.middleware.dto.BackEndEntity.Token;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TokenJwt {
    private String token;
}
