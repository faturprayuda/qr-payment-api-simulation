package co.id.middleware.entites;

import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Transaction {
    private String trxId;

    private Merchant merchant;

    private float amountPurchase;

    private Timestamp transactionDate;

    private String accountNumber;

    private String bankAccount;

    private String statusPayment;

    private String descriptionPayment;

    private Timestamp createdDate;

    private Timestamp updatedDate;
}
