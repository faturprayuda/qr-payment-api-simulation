package co.id.middleware.entites;

import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Merchant {
    private String id;

    private String merchant_name;

    private String merchant_address;

    private String merchant_phone;

    private Timestamp created_date;

    private Timestamp updated_date;
}
