package co.id.backendprocess.repositories;

import co.id.backendprocess.entities.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, String> {
    @Query("select t, m from Transaction t inner join Merchant m on t.merchantId = m.id where t.accountNumber = :accountNumber order by t.createdDate desc")
    Page<Transaction> findByAccountNumber(String accountNumber, Pageable pageable);
}
