package co.id.backendprocess.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Response {
    private String responseCode;
    private String responseMessage;
    private Object data;
}
