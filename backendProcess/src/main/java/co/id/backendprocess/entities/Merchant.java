package co.id.backendprocess.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import java.sql.Timestamp;

@Entity
@Table(name = "tb_merchant")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Merchant {
    @Id
    @Column(name = "id",unique = true)
    private String id;

    @Column(name = "merchant_name")
    private String merchant_name;

    @Column(name = "merchant_address")
    private String merchant_address;

    @Column(name = "merchant_phone")
    private String merchant_phone;

    @Column(name = "created_date")
    private Timestamp created_date;

    @Column(name = "updated_date")
    private Timestamp updated_date;
}
