package co.id.backendprocess.entities;

import jakarta.persistence.*;
import lombok.*;

import java.sql.Timestamp;

@Entity
@Table(name = "tb_transaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Transaction {
    @Id
    @Column(name = "trx_id")
    private String trxId;

    @JoinColumn(name = "merchant_id")
    private String merchantId;

    @Column(name = "amount_purchase")
    private float amountPurchase;

    @Column(name = "transaction_date")
    private Timestamp transactionDate;

    @Column(name = "account_number")
    private String accountNumber;

    @Column(name = "bank_account")
    private String bankAccount;

    @Column(name = "status_payment")
    private String statusPayment;

    @Column(name = "description_payment")
    private String descriptionPayment;

    @Column(name = "created_date")
    private Timestamp createdDate;

    @Column(name = "updated_date")
    private Timestamp updatedDate;
}
