package co.id.backendprocess.controllers;

import co.id.backendprocess.dto.Response;
import co.id.backendprocess.dto.TransactionRequest;
import co.id.backendprocess.services.GenerateTokenService;
import co.id.backendprocess.services.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1")
@RequiredArgsConstructor
public class MainController {

    public final GenerateTokenService generateTokenService;
    public final TransactionService transactionService;

    @GetMapping("/generate-token")
    ResponseEntity<Object> generateToken(){
        return generateTokenService.getToken();
    }

    @PostMapping("/tr/transactions")
    ResponseEntity<Object> transaction(@RequestBody TransactionRequest transactionRequest){
        return transactionService.getTransactions(transactionRequest);
    }

    @GetMapping("/tr/test")
    ResponseEntity<Object> output(){
        return ResponseEntity.ok(Response.builder()
                .responseCode("200")
                .responseMessage("success")
                .data("Hello World!")
                .build());
    }
}
