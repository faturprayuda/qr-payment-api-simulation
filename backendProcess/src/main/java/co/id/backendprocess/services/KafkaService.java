package co.id.backendprocess.services;

import co.id.backendprocess.entities.Transaction;
import co.id.backendprocess.repositories.MerchantRepository;
import co.id.backendprocess.repositories.TransactionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.sql.Timestamp;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaService {

    private final TransactionRepository transactionRepository;

    @KafkaListener(id = "my-topics-id", topics = "my-topics", groupId = "my-topics-group")
    public void saveTransaction(String incomingData) {
        StopWatch sw = new StopWatch();

        Transaction transaction = null;
        Timestamp ts = new Timestamp(System.currentTimeMillis());

        String reqId = null;

        try {
            sw.start();

            ObjectMapper mapper = new ObjectMapper();
            transaction = mapper.readValue(incomingData, Transaction.class);

            reqId = transaction.getTrxId();

            log.info("Incoming Data : {} | REQ-ID : [{}]", incomingData, reqId);

            transaction.setTransactionDate(ts);
            transaction.setStatusPayment("success");
            transaction.setDescriptionPayment("success");
            transaction.setCreatedDate(ts);
            transaction.setUpdatedDate(ts);

            log.info("Saving Data Transaction : {} | REQ-ID : [{}]", transaction, reqId);

            savePayment(transaction);

            sw.stop();
            log.info("Saved Data --done in {}ms | REQ-ID : [{}]", sw.getTotalTimeMillis(), reqId);
        } catch (Exception e) {
            log.error("error : {} | REQ-ID : [{}]", e.getMessage(), reqId);

            assert transaction != null;
            if (e.getMessage().contains("a foreign key constraint fails")) {
                transaction.setMerchantId(null);
            }
            transaction.setTransactionDate(ts);
            transaction.setStatusPayment("failed");
            transaction.setDescriptionPayment(e.getMessage().substring(0, 254));
            transaction.setCreatedDate(ts);
            transaction.setUpdatedDate(ts);

            savePayment(transaction);
        }

    }

    @Transactional
    public void savePayment(Transaction transaction) {
        transactionRepository.save(transaction);
    }
}
