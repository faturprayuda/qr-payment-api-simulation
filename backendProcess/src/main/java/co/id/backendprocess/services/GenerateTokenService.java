package co.id.backendprocess.services;

import co.id.backendprocess.dto.Response;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Service
@Slf4j
@RequiredArgsConstructor
public class GenerateTokenService {

    @Value("${token.signing.key}")
    private String jwtSigningKey;
    @Value("${email.user}")
    private String emailUser;

    private final RedisTemplate<String, Object> redisTemplate;

    public ResponseEntity<Object> getToken() {
        StopWatch sw = new StopWatch();

        try {
            log.info("Prepare Generate Token");
            sw.start();
            Object tokenRedis = redisTemplate.opsForValue().get("token");

            if (tokenRedis == null) {
                log.info("generate token");
                tokenRedis = Jwts.builder().setClaims(new HashMap<>()).setSubject(emailUser)
                        .setIssuedAt(new Date(System.currentTimeMillis()))
                        .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60))
                        .signWith(getSigningKey(), SignatureAlgorithm.HS256).compact();

                redisTemplate.opsForValue().set("token", tokenRedis);
                redisTemplate.expire("token", 50, TimeUnit.SECONDS);
            }

            sw.stop();
            log.info("Generate Token --done in {}ms | Token Result = {}", sw.getTotalTimeMillis(), tokenRedis);
            Map<String, Object> map = new HashMap<>();
            map.put("token", tokenRedis);

            return ResponseEntity.ok(Response.builder()
                    .responseCode("200")
                    .responseMessage("success")
                    .data(map)
                    .build());
        } catch (Exception e) {
            log.error("error: {}", e.getMessage());
            return ResponseEntity.internalServerError().body(Response.builder()
                    .responseCode("500")
                    .responseMessage("error")
                    .data(e.getMessage())
                    .build());
        }
    }

    private Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(jwtSigningKey);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    public String extractUserName(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    private <T> T extractClaim(String token, Function<Claims, T> claimsResolvers) {
        final Claims claims = extractAllClaims(token);
        return claimsResolvers.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parserBuilder().setSigningKey(getSigningKey()).build().parseClaimsJws(token)
                .getBody();
    }

    public boolean isTokenValid(String token) {
        return !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
}
