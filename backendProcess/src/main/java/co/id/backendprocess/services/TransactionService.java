package co.id.backendprocess.services;

import co.id.backendprocess.dto.Response;
import co.id.backendprocess.dto.TransactionRequest;
import co.id.backendprocess.entities.Transaction;
import co.id.backendprocess.repositories.TransactionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransactionService {

    private final TransactionRepository transactionRepository;

    @Transactional
    public ResponseEntity<Object> getTransactions(TransactionRequest transactionRequest) {
        StopWatch sw = new StopWatch();
        try{
            sw.start();
            log.info("Get Transactions from Account Number : [{}]", transactionRequest.getAccountNumber());
            Page<Transaction> transactionList = transactionRepository.findByAccountNumber(transactionRequest.getAccountNumber(), PageRequest.of(transactionRequest.getPage(), transactionRequest.getSize()));

            sw.stop();
            log.info("Get Transactions --done in {}ms", sw.getTotalTimeMillis());
            return ResponseEntity.ok(Response.builder()
                    .responseCode("200")
                    .responseMessage("success")
                    .data(transactionList.getContent())
                    .build());
        }catch (Exception e){
            log.error("error: {}", e.getMessage());
            return ResponseEntity.internalServerError().body(Response.builder()
                    .responseCode("500")
                    .responseMessage("failed")
                    .data(null)
                    .build());
        }
    }
}
