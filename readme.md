# QR Payment API Simulation

<div style="text-align: justify">
QR Payment Simulation API is a simulation API service for testing payment using QR codes. Currently, the available APIs consist of payment and total transaction functionalities. In the future, there will be updates to the API according to the needs of QR Payment simulation API.
</div>

This application utilizes a tech stack including Java SpringBoot, Redis, Kafka, ELK and the application build in microservice.

1. [How it works](#how-it-works)
2. [How to use](#how-to-use)
3. [How to check Logging from ELK](#how-to-check-logging-from-elk)

## How it works

![Flow Chart Schema](image/Untitled-2023-08-21-1718.png)

There are 4 APIs which consist of:

MS MIDDLEWARE:

- [/api/v1/payment](#)

    This API is responsible for processing payments, where the payment results will be stored in a pre-prepared MySQL database. The API works as follows:

    When this API is called, the carried data will be **published** to the **Kafka broker**, so that the backend service only needs to **subscribe** to the predefined **topic**. After that, the subscribed data will be processed and stored in the **MySQL database**.


- [/api/v1/transactions](#)

    This API is responsible for serving as a payment history by retrieving all transactions with a successful status. The working mechanism of this API is as follows:

    When this API is called, it will first validate whether there is a token available in the **Redis DB**. If not, the next step is to create a token by calling the **API** owned by the **backend service** with the path **/api/v1/generate-token**, then store it in the **Redis DB.** If yes, then the available token will be immediately retrieved and used. After the token is available, the next step is to call the **API** owned by the **backend service** with the path **/api/v1/tr/transactions**, which is responsible for retrieving data based on the **AccountNumber**.

MS BACKEND:

- [/api/v1/generate-token](#)

    This API is used to generate a token. Token that generate just has 1 minute life time, so after 1 minute, the Token will be remove from Redis and can't use again.


- [/api/v1/tr/transactions](#)

  This API is used to retrieve transaction data

## How to use
- First, clone this repo
```shell
git clone https://gitlab.com/faturprayuda/qr-payment-api-simulation
```

- Execute this code for running the Apps, please be patience until all of the container created and running well
```dockerfile
docker-compose up -d
```

- Execute the API with swagger
```text
http://localhost:8080/swagger-ui/index.html#/ -> URL for MS Midlleware
http://localhost:8081/swagger-ui/index.html#/ -> URL for MS Backend
```
---
## Test API with Swagger
- for MS Midlleware, open the link below:
```text
http://localhost:8080/swagger-ui/index.html#/
```

- i will recommend to call **/api/v1/payment** because there's no data transaction for first time we launch the apps, the Body Request will be look like below: 

```json
{
  "merchantId": "85083a0e-e06b-11ee-be7b-0242ac140002",
  "amountPurchase": 1320000,
  "accountNumber": "8907341265",
  "bankAccount": "Bank A"
}
```
For **accountNumber** there no rule, you can fill with any value as you like.

For the **merchantId** value, you can use from file: 
```text
nano ./sql/schema.sql
```

 
and for the result will be look like below:
```json
{
  "page": 0,
  "size": 0,
  "totalSize": 0,
  "responseCode": "200",
  "responseMessage": "success",
  "data": {
    "trxId": "246cbadd-dc07-4c7c-ab8c-23148edcf509",
    "merchantId": "85083a0e-e06b-11ee-be7b-0242ac140002",
    "amountPurchase": 1320000,
    "accountNumber": "8907341265",
    "bankAccount": "Bank A"
  }
}
```

- for the **/api/v1/transactions** path, here the body request:
```json
{
  "accountNumber": "8907341265",
  "page": 0,
  "size": 10
}
```
for **size** value, please fill with number more than **0**.

The response body will be look below:
```json
{
  "page": 0,
  "size": 10,
  "totalSize": 2,
  "responseCode": "200",
  "responseMessage": "success",
  "data": {
    "responseCode": "200",
    "responseMessage": "success",
    "data": [
      [
        {
          "trxId": "246cbadd-dc07-4c7c-ab8c-23148edcf509",
          "merchantId": "85083a0e-e06b-11ee-be7b-0242ac140002",
          "amountPurchase": 1320000,
          "transactionDate": "2024-03-12T17:42:56.000+00:00",
          "accountNumber": "8907341265",
          "bankAccount": "Bank A",
          "statusPayment": "success",
          "descriptionPayment": "success",
          "createdDate": "2024-03-12T17:42:56.000+00:00",
          "updatedDate": "2024-03-12T17:42:56.000+00:00"
        },
        {
          "id": "85083a0e-e06b-11ee-be7b-0242ac140002",
          "merchant_name": "Warung Sate Madura",
          "merchant_address": "Jl. Ahmad Yani No. 123",
          "merchant_phone": "081234567890",
          "created_date": "2024-03-12T08:36:15.000+00:00",
          "updated_date": "2024-03-12T08:36:15.000+00:00"
        }
      ],
      [
        {
          "trxId": "42ced39b-bf88-40d7-9ec9-06e5c6e4da5a",
          "merchantId": "85083a0e-e06b-11ee-be7b-0242ac140002",
          "amountPurchase": 1320000,
          "transactionDate": "2024-03-12T15:40:59.000+00:00",
          "accountNumber": "8907341265",
          "bankAccount": "Bank A",
          "statusPayment": "success",
          "descriptionPayment": "success",
          "createdDate": "2024-03-12T15:40:59.000+00:00",
          "updatedDate": "2024-03-12T15:40:59.000+00:00"
        },
        {
          "id": "85083a0e-e06b-11ee-be7b-0242ac140002",
          "merchant_name": "Warung Sate Madura",
          "merchant_address": "Jl. Ahmad Yani No. 123",
          "merchant_phone": "081234567890",
          "created_date": "2024-03-12T08:36:15.000+00:00",
          "updated_date": "2024-03-12T08:36:15.000+00:00"
        }
      ]
    ]
  }
}
```
---
- for MS Backend, open the link below:
```text
http://localhost:8081/swagger-ui/index.html#/
```

- please generate token first with path  **/api/v1/generate-token** because other API need Authorization that using Token, this API didn't neet request body, so the response body will be look below:

```json
{
  "responseCode": "200",
  "responseMessage": "success",
  "data": {
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTcxMDI2NjAzMCwiZXhwIjoxNzEwMjY2MDkwfQ.K6jJ7B9l0bsCvVLuC_EICycpzULeAnadrzkFD9x1vd4"
  }
}
```

- For path **/api/v1/tr/transactions**, you should fill the **Header** with key **Authorization** and value **Bearer + Token**

```text
-H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBtYWlsLmNvbSIsImlhdCI6MTcxMDI2NjI1MywiZXhwIjoxNzEwMjY2MzEzfQ.e6H-B7WprBPTLsFMNClhfipJRqTaRUfXkkOYTDsJUMs'
```

- Request body will be look like below:
```json
{
  "accountNumber": "8907341265",
  "page": 0,
  "size": 10
}
```

and for the result will be look like below:
```json
{
  "responseCode": "200",
  "responseMessage": "success",
  "data": [
    [
      {
        "trxId": "246cbadd-dc07-4c7c-ab8c-23148edcf509",
        "merchantId": "85083a0e-e06b-11ee-be7b-0242ac140002",
        "amountPurchase": 1320000,
        "transactionDate": "2024-03-12T17:42:56.000+00:00",
        "accountNumber": "8907341265",
        "bankAccount": "Bank A",
        "statusPayment": "success",
        "descriptionPayment": "success",
        "createdDate": "2024-03-12T17:42:56.000+00:00",
        "updatedDate": "2024-03-12T17:42:56.000+00:00"
      },
      {
        "id": "85083a0e-e06b-11ee-be7b-0242ac140002",
        "merchant_name": "Warung Sate Madura",
        "merchant_address": "Jl. Ahmad Yani No. 123",
        "merchant_phone": "081234567890",
        "created_date": "2024-03-12T08:36:15.000+00:00",
        "updated_date": "2024-03-12T08:36:15.000+00:00"
      }
    ],
    [
      {
        "trxId": "42ced39b-bf88-40d7-9ec9-06e5c6e4da5a",
        "merchantId": "85083a0e-e06b-11ee-be7b-0242ac140002",
        "amountPurchase": 1320000,
        "transactionDate": "2024-03-12T15:40:59.000+00:00",
        "accountNumber": "8907341265",
        "bankAccount": "Bank A",
        "statusPayment": "success",
        "descriptionPayment": "success",
        "createdDate": "2024-03-12T15:40:59.000+00:00",
        "updatedDate": "2024-03-12T15:40:59.000+00:00"
      },
      {
        "id": "85083a0e-e06b-11ee-be7b-0242ac140002",
        "merchant_name": "Warung Sate Madura",
        "merchant_address": "Jl. Ahmad Yani No. 123",
        "merchant_phone": "081234567890",
        "created_date": "2024-03-12T08:36:15.000+00:00",
        "updated_date": "2024-03-12T08:36:15.000+00:00"
      }
    ]
  ]
}
```
---
## How to check Logging from ELK

- Check status Logstash first, make sure teh status is **green**


  ![Flow Chart Schema](image/logstash-status.png)

- Open Kibana and got to Discover page, for the first time we using ELK, please **create** the **data view**

![Flow Chart Schema](image/kibana-step1.png)

- create name data view like the index that showing in the right panel


  ![Flow Chart Schema](image/kibana-step2.png)

- change the data view with the data view that has been created before, and the logging apps will be shown


  ![Flow Chart Schema](image/kibana-step3.png)




