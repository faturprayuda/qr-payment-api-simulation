CREATE DATABASE IF NOT EXISTS qr_payment;

CREATE TABLE qr_payment.tb_merchant (
                             id VARCHAR(36) PRIMARY KEY,
                             merchant_name VARCHAR(255) NOT NULL,
                             merchant_address VARCHAR(255) NOT NULL,
                             merchant_phone VARCHAR(20) NOT NULL,
                             created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                             updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE qr_payment.tb_transaction (
                                trx_id VARCHAR(36) PRIMARY KEY,
                                merchant_id VARCHAR(36),
                                amount_purchase DECIMAL(10, 2) NOT NULL,
                                transaction_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                account_number VARCHAR(20) NOT NULL,
                                bank_account VARCHAR(255) NOT NULL,
                                status_payment ENUM('success', 'failed') NOT NULL,
                                description_payment VARCHAR(255),
                                created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                FOREIGN KEY (merchant_id) REFERENCES tb_merchant(id)
);

INSERT INTO qr_payment.tb_merchant (id, merchant_name, merchant_address, merchant_phone, created_date, updated_date) VALUES
                                                                                                             ("85083a0e-e06b-11ee-be7b-0242ac140002", 'Warung Sate Madura', 'Jl. Ahmad Yani No. 123', '081234567890', NOW(), NOW()),
                                                                                                             ("85127b97-e06b-11ee-be7b-0242ac140002", 'Bakso Pak Slamet', 'Jl. Gajah Mada No. 45', '085678901234', NOW(), NOW()),
                                                                                                             ("8512874d-e06b-11ee-be7b-0242ac140002", 'Nasi Goreng Bu Tati', 'Jl. Sudirman No. 78', '081345678901', NOW(), NOW()),
                                                                                                             ("8512890d-e06b-11ee-be7b-0242ac140002", 'Es Teh Bang Udin', 'Jl. Pahlawan No. 12', '081234567890', NOW(), NOW()),
                                                                                                             ("85128a0b-e06b-11ee-be7b-0242ac140002", 'Mie Ayam Kang Ade', 'Jl. Pemuda No. 56', '085678901234', NOW(), NOW()),
                                                                                                             ("85128b47-e06b-11ee-be7b-0242ac140002", 'Soto Ayam Mbak Endang', 'Jl. Kartini No. 34', '081345678901', NOW(), NOW()),
                                                                                                             ("85128c43-e06b-11ee-be7b-0242ac140002", 'Bakmi Jawa Mas Priyono', 'Jl. Diponegoro No. 67', '081234567890', NOW(), NOW()),
                                                                                                             ("85128d62-e06b-11ee-be7b-0242ac140002", 'Pecel Lele Mbak Siti', 'Jl. Surya Sumantri No. 78', '085678901234', NOW(), NOW()),
                                                                                                             ("85128e8e-e06b-11ee-be7b-0242ac140002", 'Martabak Telor Bang Joko', 'Jl. A. Yani No. 90', '081345678901', NOW(), NOW()),
                                                                                                             ("85128f97-e06b-11ee-be7b-0242ac140002", 'Roti Bakar Bu Dewi', 'Jl. Thamrin No. 34', '081234567890', NOW(), NOW());